import json
import concurrent.futures

import pytest

from .util.constants import Constants
from .util.endpoints import request_sizerecommendation

from .util.asset_urls import (
    female_garment_urls,
    female_bm3dstate_url,
    female_bm3dstate_relpath,
    male_garment_urls,
    male_bm3dstate_url,
    male_bm3dstate_relpath,
)


def _test_all_urls(garment_url: str, bm3dstate_url: str) -> None:
    size_table_url = Constants.test_http_server_url() + "/configs/size_table_demo_pants.json"
    measurements_config = Constants.test_http_server_url() + "/configs/size_recommendation_measurement_config.json"
    payload = {
        "garmentUrl": garment_url,
        "bodyModelState": bm3dstate_url,
        "sizeTable": size_table_url,
        "measurementsConfig": measurements_config,
    }

    response = request_sizerecommendation(data=payload, params={"createSizeVisualisationMesh": False})
    response_json = json.loads(response.content)

    assert response_json


def _test_optional_info(garment_url: str, bm3dstate_url: str) -> None:
    size_table_url = Constants.test_http_server_url() + "/configs/size_table_demo_pants.json"
    measurements_config = Constants.test_http_server_url() + "/configs/size_recommendation_measurement_config.json"
    payload = {
        "garmentUrl": garment_url,
        "bodyModelState": bm3dstate_url,
        "sizeTable": size_table_url,
        "measurementsConfig": measurements_config,
        "size": "32x32",
        "colorTight": '{"r": 1.0, "g": 0.0, "b": 1.0}',
        "colorLoose": '{"r": 1.0, "g": 0.0, "b": 1.0}',
    }

    response = request_sizerecommendation(data=payload, params={"createSizeVisualisationMesh": False})
    response_json = json.loads(response.content)

    assert response_json


def _test_bodymodelstate_blob(garment_url: str, bm_path: str) -> None:
    files = {"bodyModelState": ("bodyModelState.bin", open(bm_path, "rb"), "application/octet-stream")}
    size_table_url = Constants.test_http_server_url() + "/configs/size_table_demo_pants.json"
    measurements_config = Constants.test_http_server_url() + "/configs/size_recommendation_measurement_config.json"
    payload = {
        "garmentUrl": garment_url,
        "sizeTable": size_table_url,
        "measurementsConfig": measurements_config,
    }

    response = request_sizerecommendation(data=payload, files=files, params={"createSizeVisualisationMesh": False})

    assert json.loads(response.content)


@pytest.mark.parametrize("garment_url", female_garment_urls)
def test_all_urls_female(garment_url: str) -> None:
    _test_all_urls(garment_url, female_bm3dstate_url)


@pytest.mark.parametrize("garment_url", male_garment_urls)
def test_all_urls_male(garment_url: str) -> None:
    _test_all_urls(garment_url, male_bm3dstate_url)


@pytest.mark.parametrize("garment_url", female_garment_urls)
def test_optional_info_female(garment_url: str) -> None:
    _test_optional_info(garment_url, female_bm3dstate_url)


@pytest.mark.parametrize("garment_url", male_garment_urls)
def test_optional_info_male(garment_url: str) -> None:
    _test_optional_info(garment_url, male_bm3dstate_url)


@pytest.mark.parametrize("garment_url", female_garment_urls)
def test_bodymodelstate_blob_female(garment_url: str) -> None:
    bm_path = "tests/assets/" + female_bm3dstate_relpath
    _test_bodymodelstate_blob(garment_url, bm_path)


@pytest.mark.parametrize("garment_url", male_garment_urls)
def test_bodymodelstate_blob_male(garment_url: str) -> None:
    bm_path = "tests/assets/" + male_bm3dstate_relpath
    _test_bodymodelstate_blob(garment_url, bm_path)


param_concurrent_requests = [10]


@pytest.mark.parametrize("concurrent_requests", param_concurrent_requests)
def test_concurrent_mixed_requests(concurrent_requests: int) -> None:
    with concurrent.futures.ThreadPoolExecutor(max_workers=concurrent_requests) as executor:
        futures = []

        for _ in range(concurrent_requests):
            future = executor.submit(test_bodymodelstate_blob_female, female_garment_urls[0])
            futures.append(future)

            future = executor.submit(test_all_urls_female, female_garment_urls[0])
            futures.append(future)

            future = executor.submit(test_optional_info_female, female_garment_urls[0])
            futures.append(future)

            future = executor.submit(test_bodymodelstate_blob_male, male_garment_urls[0])
            futures.append(future)

            future = executor.submit(test_all_urls_male, male_garment_urls[0])
            futures.append(future)

            future = executor.submit(test_optional_info_male, male_garment_urls[0])
            futures.append(future)

        for future in futures:
            future.result()
