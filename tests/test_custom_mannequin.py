import json
import concurrent.futures

from typing import List, Optional, Dict

import pytest

from .util.endpoints import request_custom_mannequin

pbmIds = ["DAZ_Genesis8Male_default", "DAZ_Genesis8Female_default"]


def assert_return(payload: Dict, requested_files: List[str], files: Optional[Dict] = None) -> None:
    response = request_custom_mannequin(data=payload, params={"file": requested_files}, files=files)
    response_json = json.loads(response.content)
    files_returned = ["found: " + file["id"] for file in response_json["files"] if file["id"] in requested_files]
    assert len(files_returned) == len(requested_files)


@pytest.mark.parametrize("pbmId", pbmIds)
def test_minimal_request_only_model(pbmId: str) -> None:
    assert_return(payload={"bodyModelID": pbmId}, requested_files=["model"])


@pytest.mark.parametrize("pbmId", pbmIds)
def test_minimal_request_only_bodymodelstate(pbmId: str) -> None:
    assert_return(payload={"bodyModelID": pbmId}, requested_files=["model", "bodymodelstate"])


@pytest.mark.parametrize("pbmId", pbmIds)
def test_minimal_request_all_files(pbmId: str) -> None:
    assert_return(payload={"bodyModelID": pbmId}, requested_files=["bodymodelstate"])


def test_full_request_female() -> None:
    pbmId = "DAZ_Genesis8Female_default"
    pose_config_path = "tests/assets/poses/female.bm3dpose"
    payload = {
        "bodyModelID": pbmId,
        "baseShapeType": "Endomorph",
        "measurementTargets": [
            '{"id": "shoulder_length", "value": 0.55}',
            '{"id": "waist_circumference", "value": 0.55}',
            '{"id": "chest_circumference", "value": 0.55}',
            '{"id": "hip_circumference", "value": 0.55}',
        ],
        "learningRate": 0.1,
        "maxIterations": 1000,
        "factorThreshold": 0.00001,
    }
    files = {"pose": ("femalePose.bm3dpose", open(pose_config_path, "rb"), "application/octet-stream")}

    assert_return(payload=payload, requested_files=["model"], files=files)


def test_full_request_male() -> None:
    pbmId = "DAZ_Genesis8Male_default"
    pose_config_path = "tests/assets/poses/male.bm3dpose"
    payload = {
        "bodyModelID": pbmId,
        "baseShapeType": "Endomorph",
        "measurementTargets": [
            '{"id": "shoulder_length", "value": 0.55}',
            '{"id": "waist_circumference", "value": 0.55}',
            '{"id": "chest_circumference", "value": 0.55}',
            '{"id": "hip_circumference", "value": 0.55}',
        ],
        "learningRate": 0.1,
        "maxIterations": 1000,
        "factorThreshold": 0.00001,
    }
    files = {"pose": ("male.bm3dpose", open(pose_config_path, "rb"), "application/octet-stream")}

    assert_return(payload=payload, requested_files=["model"], files=files)


param_concurrent_requests = [10]


@pytest.mark.parametrize("concurrent_requests", param_concurrent_requests)
def test_concurrent_mixed_requests(concurrent_requests: int) -> None:
    with concurrent.futures.ThreadPoolExecutor(max_workers=concurrent_requests) as executor:
        futures = []

        for _ in range(concurrent_requests):
            future = executor.submit(test_minimal_request_only_model, pbmIds[1])
            futures.append(future)

            future = executor.submit(test_minimal_request_only_model, pbmIds[0])
            futures.append(future)

            future = executor.submit(test_full_request_female)
            futures.append(future)

            future = executor.submit(test_full_request_male)
            futures.append(future)

            # do 2 more of the same request
            future = executor.submit(test_minimal_request_only_model, pbmIds[1])
            futures.append(future)

            future = executor.submit(test_minimal_request_only_model, pbmIds[0])
            futures.append(future)

        for future in futures:
            future.result()