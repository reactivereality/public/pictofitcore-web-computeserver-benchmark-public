import json
import concurrent.futures

import pytest

from .util.endpoints import request_bodymodelIds


def test_single_request() -> None:
    response = request_bodymodelIds()
    response_json = json.loads(response.content)
    bodymodels = response_json["bodyModelIDs"]
    assert len(bodymodels) == 2
    assert "DAZ_Genesis8Male_default" in bodymodels


param_concurrent_requests = [5, 10, 20]


@pytest.mark.parametrize("concurrent_requests", param_concurrent_requests)
def test_concurrent_requests(concurrent_requests: int) -> None:
    with concurrent.futures.ThreadPoolExecutor(max_workers=concurrent_requests) as executor:
        futures = []

        for _ in range(concurrent_requests):
            future = executor.submit(test_single_request)
            futures.append(future)

        for future in futures:
            future.result()
