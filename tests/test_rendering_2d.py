import concurrent.futures

import pytest

from .util.endpoints import request_slotlayout_rendering
from .util.constants import Constants


def test_empty_slotlayout() -> None:
    json = {"size": {"width": 100, "height": 100}}
    assert request_slotlayout_rendering(json)


def test_backgroundcolor() -> None:
    json = {"size": {"width": 100, "height": 100}, "backgroundColor": {"r": 1, "g": 0, "b": 0}}
    assert request_slotlayout_rendering(json)


def test_fullsized_quad() -> None:
    json = {
        "size": {"width": 1024, "height": 1024},
        "backgroundColor": {"r": 0.9, "g": 0.9, "b": 0.9},
        "slots": [
            {
                "type": "Quad",
                "scalingMode": "AspectFit",
                "frame": {"left": 0, "top": 0, "width": 1024, "height": 1024},
                "quadUrl": Constants.test_http_server_url() + "/images/rr_logo.png",
            }
        ],
    }
    assert request_slotlayout_rendering(json)


def test_all_slots() -> None:
    width = 1024
    height = 1024

    json = {
        "size": {"width": width, "height": height},
        "backgroundColor": {"r": 0.9, "g": 0.9, "b": 0.9},
        "slots": [
            {
                "type": "Quad",
                "scalingMode": "AspectFit",
                "frame": {"left": 0, "top": height / 2, "width": width / 2, "height": height / 2},
                "quadUrl": Constants.test_http_server_url() + "/images/rr_logo.png",
            },
            {
                "type": "TryOn",
                "scalingMode": "AspectFit",
                "frame": {"left": width / 2, "top": 0, "width": width / 2, "height": 1024},
                "avatarUrl": Constants.test_http_server_url() + "/avatars/2D/martina.avatar",
                "garmentUrls": [
                    Constants.test_http_server_url() + "/garments/2D/pants.garment",
                    Constants.test_http_server_url() + "/garments/2D/blouse.garment",
                ],
            },
            {
                "type": "Garment",
                "scalingMode": "AspectFit",
                "frame": {"left": 0, "top": 0, "width": width / 2, "height": height / 2},
                "garmentUrl": Constants.test_http_server_url() + "/garments/2D/sweater.garment",
            },
        ],
    }
    assert request_slotlayout_rendering(json)

param_concurrent_requests = [10]


@pytest.mark.parametrize("concurrent_requests", param_concurrent_requests)
def test_concurrent_mixed_requests(concurrent_requests: int) -> None:
    with concurrent.futures.ThreadPoolExecutor(max_workers=concurrent_requests) as executor:
        futures = []

        for _ in range(concurrent_requests):
            future = executor.submit(test_all_slots)
            futures.append(future)

            future = executor.submit(test_backgroundcolor)
            futures.append(future)

            future = executor.submit(test_empty_slotlayout)
            futures.append(future)

            # add up to 6 requests
            future = executor.submit(test_all_slots)
            futures.append(future)

            future = executor.submit(test_backgroundcolor)
            futures.append(future)

            future = executor.submit(test_empty_slotlayout)
            futures.append(future)

        for future in futures:
            future.result()
