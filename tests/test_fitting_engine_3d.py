import concurrent.futures
import json

from typing import List

import pytest

from .util.endpoints import request_fittingengine3d
from .util.asset_urls import female_garment_urls, female_bm3dstate_url, female_bm3dstate_relpath
from .util.asset_urls import male_garment_urls, male_bm3dstate_url, male_bm3dstate_relpath

param_garment_urls_male = [
    [male_garment_urls[0]],
    [male_garment_urls[0], male_garment_urls[1]],
    [male_garment_urls[0], male_garment_urls[1], male_garment_urls[0]]
]

param_garment_urls_female = [
    [female_garment_urls[0]],
    [female_garment_urls[0], female_garment_urls[1]],
    [female_garment_urls[0], female_garment_urls[1], female_garment_urls[0]]
]

@pytest.mark.parametrize("garment_urls", param_garment_urls_female)
def test_garment_urls_female(garment_urls: List[str]) -> None:
    payload = {
        "garmentUrls": garment_urls,
        "bodyModelState": female_bm3dstate_url,
    }

    response = request_fittingengine3d(data=payload)
    assert json.loads(response.content)

@pytest.mark.parametrize("garment_urls", param_garment_urls_male)
def test_garment_urls_male(garment_urls: List[str]) -> None:
    payload = {
        "garmentUrls": garment_urls,
        "bodyModelState": male_bm3dstate_url,
    }

    response = request_fittingengine3d(data=payload)
    assert json.loads(response.content)


def test_bodymodelstate_blob_female() -> None:
    bm_path = "tests/assets/" + female_bm3dstate_relpath
    files = {"bodyModelState": ("bodyModelState.bin", open(bm_path, "rb"), "application/octet-stream")}
    payload = {
        "garmentUrls": [female_garment_urls[0]],
    }

    response = request_fittingengine3d(data=payload, files=files)
    assert json.loads(response.content)

def test_bodymodelstate_blob_male() -> None:
    bm_path = "tests/assets/" + male_bm3dstate_relpath
    files = {"bodyModelState": ("bodyModelState.bin", open(bm_path, "rb"), "application/octet-stream")}
    payload = {
        "garmentUrls": [male_garment_urls[0]],
    }

    response = request_fittingengine3d(data=payload, files=files)
    assert json.loads(response.content)


param_concurrent_requests = [10]


@pytest.mark.parametrize("concurrent_requests", param_concurrent_requests)
def test_concurrent_mix_requests(concurrent_requests: int) -> None:
    with concurrent.futures.ThreadPoolExecutor(max_workers=concurrent_requests) as executor:
        futures = []

        for _ in range(concurrent_requests):

            for garment_urls in param_garment_urls_male:
                future = executor.submit(test_garment_urls_male, garment_urls)
                futures.append(future)

            for garment_urls in param_garment_urls_female:
                future = executor.submit(test_garment_urls_female, garment_urls)
                futures.append(future)

        for future in futures:
            future.result()
