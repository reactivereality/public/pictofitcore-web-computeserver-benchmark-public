import json
import pytest

from .util.endpoints import request_measurements


pbmIds = ["DAZ_Genesis8Male_default", "DAZ_Genesis8Female_default"]


@pytest.mark.parametrize("pbmId", pbmIds)
def test_single_request(pbmId: str) -> None:
    response = request_measurements(params={"pbmId": pbmId})
    response_json = json.loads(response.content)
    assert len(response_json["measurements"]) == 4


def test_invalid_pbmId() -> None:
    response = request_measurements(params={"pbmId": "invalid-pbmid"}, throw_error = False)
    assert response.status_code == 400  # Bad Request
