from pathlib import Path
import os

from typing import Optional, Dict


class Constants:
    # this should equal the tests folder
    _work_dir = os.getcwdb()
    _base_path = Path(__file__).resolve().parent.parent

    @staticmethod
    def compute_server_url() -> str:
        env_port = os.environ.get("PORT")
        port = int(env_port) if env_port and env_port.isdigit() else 8000
        # docker dns
        return "http://computeserver:{}".format(port)

    @staticmethod
    def test_http_server_url() -> str:
        return "http://computeserver-benchmark:8001"

    @staticmethod
    def auth_header() -> Optional[Dict]:
        token = os.environ.get("AUTHENTICATION_TOKEN")
        if token:
            return {"Authorization": "Bearer " + token}
        return None
