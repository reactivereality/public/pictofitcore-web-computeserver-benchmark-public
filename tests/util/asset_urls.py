from .constants import Constants

female_bm3dstate_relpath = "/bodymodelstates/female.bm3dstate"
male_bm3dstate_relpath = "/bodymodelstates/male.bm3dstate"

female_bm3dstate_url = Constants.test_http_server_url() + female_bm3dstate_relpath
male_bm3dstate_url = Constants.test_http_server_url() + male_bm3dstate_relpath

female_garment_urls = [
    Constants.test_http_server_url() + "/garments/3D/female/garment-1.gm3d",
    Constants.test_http_server_url() + "/garments/3D/female/garment-2.gm3d",
    Constants.test_http_server_url() + "/garments/3D/female/275f9e54-2771-478d-9e2e-1956f2b1bf8b.gm3d",
    Constants.test_http_server_url() + "/garments/3D/female/73e80c81-79d7-4a07-a8fc-4bfc62280134.gm3d",
    Constants.test_http_server_url() + "/garments/3D/female/09df54c0-ddd3-436d-98ad-8c78e1b3fd81.gm3d",
    Constants.test_http_server_url() + "/garments/3D/female/932a342c-97a3-40a8-823b-61ac0dc0545c.gm3d",
]

male_garment_urls = [
    Constants.test_http_server_url() + "/garments/3D/male/garment-3.gm3d",
    Constants.test_http_server_url() + "/garments/3D/male/75ea2850-622c-476c-bfb0-010a61081e8a.gm3d",
    Constants.test_http_server_url() + "/garments/3D/male/c600af71-8b68-4dde-a48f-2c3291960c00.gm3d",
    Constants.test_http_server_url() + "/garments/3D/male/bb051368-e17a-436d-af50-b4e2d84bd9c9.gm3d",
    Constants.test_http_server_url() + "/garments/3D/male/371dd892-3d84-4ec8-9670-c3d99a744a30.gm3d",
]