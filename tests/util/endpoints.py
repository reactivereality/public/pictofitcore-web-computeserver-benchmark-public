from typing import Dict, Optional

from requests import Response, get, post
import base64

from .constants import Constants


def request_bodymodelIds() -> Response:
    response = get(Constants.compute_server_url() + "/bodymodelids", headers=Constants.auth_header())

    if not response:
        msg = "{}".format(response.__dict__)
        raise RuntimeError(msg)

    return response


def request_custom_mannequin(data: Dict, params: Dict, files: Optional[Dict] = None) -> Response:
    response = post(
        Constants.compute_server_url() + "/compute/mannequin",
        headers=Constants.auth_header(),
        data=data,
        files=files,
        params=params,
    )

    if not response:
        msg = "{}".format(response.__dict__)
        raise RuntimeError(msg)

    return response


def request_fittingengine3d(data: Dict, files: Optional[Dict] = None) -> Response:
    response = post(
        Constants.compute_server_url() + "/compute/virtualtryon3d",
        headers=Constants.auth_header(),
        data=data,
        files=files,
    )

    if not response:
        msg = "{}".format(response.__dict__)
        raise RuntimeError(msg)

    return response


def request_measurements(params: Dict, throw_error: bool = True) -> Response:
    response = get(
        Constants.compute_server_url() + "/measurements",
        headers=Constants.auth_header(),
        params=params,
    )

    if not response and throw_error:
        msg = "{}".format(response.__dict__)
        raise RuntimeError(msg)

    return response


def request_slotlayout_rendering(json: Dict) -> str:
    response = post(
        Constants.compute_server_url() + "/render/slotlayout", headers=Constants.auth_header(), json=json
    )

    if not response:
        msg = "{}".format(response.__dict__)
        raise RuntimeError(msg)

    return base64.b64encode(response.content).decode("utf-8")


def request_shape_types() -> Response:
    response = get(Constants.compute_server_url() + "/shapetypes", headers=Constants.auth_header())

    if not response:
        msg = "{}".format(response.__dict__)
        raise RuntimeError(msg)

    return response


def request_sizerecommendation(data: Dict, params: Dict, files: Optional[Dict] = None) -> Response:
    response = post(
        Constants.compute_server_url() + "/compute/sizerecommendation3d",
        headers=Constants.auth_header(),
        data=data,
        files=files,
        params=params,
    )

    if not response:
        msg = "{}".format(response.__dict__)
        raise RuntimeError(msg)

    return response
