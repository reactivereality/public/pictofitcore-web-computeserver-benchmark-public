import json
import concurrent.futures

import pytest

from .util.endpoints import request_shape_types


def test_single_request() -> None:
    response = request_shape_types()
    response_json = json.loads(response.content)
    shape_types = response_json["shapeTypes"]
    assert len(shape_types) == 3
    assert "Endomorph" in shape_types



param_concurrent_requests = [5, 10, 20]


@pytest.mark.parametrize("concurrent_requests", param_concurrent_requests)
def test_concurrent_requests(concurrent_requests: int) -> None:
    with concurrent.futures.ThreadPoolExecutor(max_workers=concurrent_requests) as executor:
        futures = []

        for _ in range(concurrent_requests):
            future = executor.submit(test_single_request)
            futures.append(future)

        for future in futures:
            future.result()
