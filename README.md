# PictofitCore-Web-ComputeServer-Benchmark

Collection of tests & scripts to benchmark the WebSDK-ComputeServer performance. 
## Setup
Bootstrap your VM / Environment by executing: `./setup_host.sh`

NOTE: After the setup, a `reboot` of the host machine will be requested, to ensure docker is working properly!

Login into the gitlab docker registry:
`docker login registry.gitlab.com -u $GITLAB_USER -p $GITLAB_API_TOKEN`

## Run Benchmark
Execute the `start.sh` script. For each testcase the following files will be generated: 
* `stats_<test_name>.txt` - contains snapshots every couple of seconds with the average CPU and memory usage. 
* `stats_<test_name>_summary.txt` - contains various aggreated results like average CPU/memory consumption of the whole test or the runtime.

NOTE: This script will not work on OSX! 

## Extend the testset
The `tests` directory holds a bunch of tests. These tests are written in python with the `pytest` library. 
You can add new tests and assets there. Once you coded your new tests, don't forget to add them to the `start.sh` script.
The naming convention for executing single tests of a set of tests looks like the following:
`tests/<test_file>.py::<test_name>`
E.g.:
`tests/test_custom_mannequin.py::test_concurrent_mixed_requests[10]`
