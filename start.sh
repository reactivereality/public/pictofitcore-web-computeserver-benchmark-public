#!/bin/bash

analyze_stats_file() {
  # call with test name, stats log file param like "stats.txt" and # of tests that were executed
  test_name="$1"
  stats_file="$2"
  num_tests="$3"

  num_stats_entries=0
  sum_cpu=0
  sum_mem=0
  min_cpu=1000000
  max_cpu=0
  min_mem=1000000
  max_mem=0

  # read all lines of stats file
  while read -r line; do
      # split line into an array
      arr=($line)

      # check if line starts with "computeserver"
      if [ ${arr[0]} == "computeserver" ]; then
        # second array element is CPU utilization - remove % at the end
        cpu=${arr[1]::-1}

        # third array element is memory utilization - remove % at the end
        mem=${arr[2]::-1}

        sum_cpu=`echo "$sum_cpu + $cpu" | bc -l`
        sum_mem=`echo "$sum_mem + $mem" | bc -l`

        if (( `echo "$cpu < $min_cpu" | bc -l` )); then
          min_cpu=$cpu
        fi
        if (( `echo "$cpu > $max_cpu" | bc -l` )); then
          max_cpu=$cpu
        fi

        if (( `echo "$mem < $min_mem" | bc -l` )); then
          min_mem=$mem
        fi
        if (( `echo "$mem > $max_mem" | bc -l` )); then
          max_mem=$mem
        fi      

        num_stats_entries=$((num_stats_entries+1))

      # runtime entry
      elif [ ${arr[0]} == "runtime:" ]; then
        # second array element is runtime - remove [s] at the end
        runtime=${arr[1]::-3}
      fi
  done < "$stats_file"

  avg_cpu=`echo "$sum_cpu / $num_stats_entries" | bc -l`
  avg_mem=`echo "$sum_mem / $num_stats_entries" | bc -l`
  avg_runtime=`echo "$runtime / $num_tests" | bc -l`

  echo "-------------------------------------------------------------------------"
  echo "test name      : $test_name"
  echo "test #         : $num_tests"
  echo "test stats file: $stats_file"
  echo "---"
  echo "min_cpu: $min_cpu[%]"
  echo "max_cpu: $max_cpu[%]"
  echo "avg_cpu: $avg_cpu[%]"
  echo "---"
  echo "min_mem: $min_mem[%]"
  echo "max_mem: $max_mem[%]"
  echo "avg_mem: $avg_mem[%]"
  echo "---"
  echo "runtime: $runtime[s]"
  echo "avg_run: $avg_runtime[s]"
  echo "-------------------------------------------------------------------------"
}

start_stats() {
  # call with stats log file param like "stats.txt"
  while true; do docker stats --no-stream --format "{{.Name}} {{.CPUPerc}} {{.MemPerc}} {{.MemUsage}}" | grep "computeserver " >> $1; done
}

start_stats_background() {
  # call with stats log file param like "stats.txt" and pid file like "stats_pid.txt"
  start_stats $1 &
  # echo $! > $2
  pid_stats_background=$!
}

start_test () {
  test_name=$1
  pytest_name=$2
  test_count=$3
  base_log_file="stats"
  log_file_summary="${base_log_file}_${test_name}_summary.txt"
  log_file="${base_log_file}_${test_name}.txt"

  rm -f $log_file
  start_stats_background $log_file
  start=`date +%s.%N`
  docker-compose exec computeserver-benchmark pytest -s $pytest_name
  end=`date +%s.%N`
  runtime=$( echo "$end - $start" | bc -l )
  kill $pid_stats_background >/dev/null 2>&1
  echo "runtime: ${runtime}[s]" >> $log_file

  analyze_stats_file $test_name $log_file $test_count > $log_file_summary
}

# reset docker-compose and start asset server
docker-compose down
docker-compose up -d
docker exec computeserver-benchmark ./tests/assets/run_server.sh &

# put your tests to be executed here
start_test "fitting" "tests/test_fitting_engine_3d.py::test_concurrent_mix_requests[10]" 10
start_test "rendering_2d" "tests/test_rendering_2d.py::test_concurrent_mixed_requests[10]" 10
start_test "size_recommendation_3d" "tests/test_size_recommendation.py::test_concurrent_mixed_requests[10]" 10
start_test "custom_mannequin" "tests/test_custom_mannequin.py::test_concurrent_mixed_requests[10]" 10

docker-compose down
